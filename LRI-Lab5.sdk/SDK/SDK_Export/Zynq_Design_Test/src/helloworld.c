/*
 * Copyright (c) 2009-2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xgpio.h"
#include "xzbroji.h"
//potrebno je importati headere za rad s gpio slopovima i xzbroji skolpom

//pocetak glavne funkcije
int main()
{
	//inicijalizacija pokazivaca na stukture za...

	//... zbroji sklop
	XZbroji iPtr;
	//... dip sklopke i led indikatore
	XGpio dip, led;
	// inicijalizacija podataka za racunske operacije i rezultat
	u32 a_b = 0;
	u32 a, b;
	u32 result = 0;

	// inicijalizacija platforme
    init_platform();

    //... hello world...
    print("Hello World\n\r");

    //inicijalizacija sklopa za zbrajanje na njegovoj base adresi
    XZbroji_Initialize(&iPtr, XPAR_ZBROJI_0_DEVICE_ID);

    //inicijalizacija led indikatora na njihovoj base adresi
    XGpio_Initialize(&led, XPAR_AXI_GPIO_0_DEVICE_ID);

    //inicijalizacija dip sklopki na njihovoj base adresi
    XGpio_Initialize(&dip, XPAR_AXI_GPIO_1_DEVICE_ID);

    //kontrolni print
    print ("Init!\n\r");

    XZbroji_Start(&iPtr);

    //postavljanje smjera gpio izlaznih pinova za led indikatore
    XGpio_SetDataDirection(&led, 1, 0x00000000);
    // postavljanje smjera gpio ulaznih pinova za dip sklopke
    XGpio_SetDataDirection(&dip, 1, 0x000000FF);

    //kontrolni print
    print ("Start\n\r");

    // provjera spremnosti sklopa za zbrajanje
    // kada funkcija vrati 1, sklop je spreman za upis podataka
    while (!XZbroji_IsReady(&iPtr));

    //kontrolni print
    print ("Spreman sam!\n\r");

    //pocetak petlje u kojoj se odvija citanje sa sklopki i pisanje na led indikatore
    while (1) {

    	//citanje 8-bit podataka sa dip sklopki i spremanje u pomocnu varijablu
    	a_b = XGpio_DiscreteRead(&dip, 1);

    	//maskiranje pomocne varijable u svrhu dobivanja vrijednosti varijabli...
    	a = (a_b & 0x0000000f);
    	b = (a_b & 0x000000f0);
    	//... i posmak druge varijable
    	b = (b >> 4);

    	//postavljanje varijablu za zbrajanje, procitanih s dip sklopki...
    	//...na ulaze A i B sklopa za zbrajanje
		XZbroji_SetA(&iPtr, a);
		XZbroji_SetB(&iPtr, b);

		// nakon sto sklop za zbrajanje obavi svoj posao, spremamo rezultat u varijablu
		result = XZbroji_GetReturn(&iPtr);

		// rezultat ispisujemo na led indikatore.
		XGpio_DiscreteWrite(&led, 1, result);
    }
    return 0;
}
